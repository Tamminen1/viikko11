/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko11;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.web.WebView;

/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Button button;
    @FXML
    private TextField textfield;
    @FXML
    private WebView web;
    @FXML
    private Button refresh;
    @FXML
    private Button order;
    @FXML
    private Button change;
    @FXML
    private Button back;
    @FXML
    private Button forward;
    private ArrayList<String> list = new ArrayList();
    private int x = 0;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        if(textfield.getText().trim()!=null) {
            if(textfield.getText().trim().equals("index.html")==true) {
                web.getEngine().load(getClass().getResource("index.html").toExternalForm());
                list.add(getClass().getResource("index.html").toExternalForm());
            }
            else {
                web.getEngine().load("http://"+textfield.getText().trim());
                list.add("http://"+textfield.getText().trim());
            }
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().load("http://www.lut.fi");
        list.add("http://www.lut.fi");
    }    

    @FXML
    private void refreshButton(ActionEvent event) {
        web.getEngine().reload();
    }

    @FXML
    private void orderButton(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void changeButton(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }

    @FXML
    private void backButton(ActionEvent event) {
        if((list.size()-2-x)>=0) {
            web.getEngine().load(list.get(list.size()-2-x));
            x++;
        }
    }

    @FXML
    private void forwardButton(ActionEvent event) {
        if(x>0) {
            web.getEngine().load(list.get(list.size()-x));
            x--;
        }
    }
    
}
